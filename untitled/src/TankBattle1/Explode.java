package TankBattle1;

import java.awt.*;

public class Explode {
    int x, y;
    private boolean live = true;
    int[] diameter = {4, 7, 12, 18, 26, 32, 49, 30, 14, 6};
    int step = 0;

    public Explode(int x, int y) {
        this.x = x; this.y = y;
    }

    public void draw(Graphics g) {
        if(!live) {
            Tank.listExplode.remove(this);
            return;
        }
        if(step == diameter.length) {
            live = false;
            step = 0;
            return;
        }
        Color c = g.getColor();
        g.setColor(Color.ORANGE);
        g.fillOval(x-diameter[step]/2+ Tank.WIDTH/2, y-diameter[step]/2+ Tank.HEIGHT/2, diameter[step], diameter[step]);
        g.setColor(c);
        step ++;
    }
}