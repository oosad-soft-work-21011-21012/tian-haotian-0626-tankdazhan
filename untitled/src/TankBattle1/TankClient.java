package TankBattle1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;


public class TankClient extends JFrame {
    int x = 50, y = 50;
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HIGHT = 600;
    public Tank tank = new Tank(x, y, true);
    static ArrayList<Tank> enemyTankList = new ArrayList<>();
    private Blood blood = new Blood();
    Wall wall1 = new Wall(150, 20, 1000, 30);
    Wall wall2 = new Wall(10, 500, 1000, 100);
    Image offScreenImage = null;

    @Override
    public void paint(Graphics g) {
        if (!tank.isLive()) {
            g.setFont(new Font("SansSerif", Font.BOLD, 100));
            g.setColor(Color.RED);
            g.drawString("YOU DIE", 200, 200);
            return;
        } else if (enemyTankList.size() == 0) {
            g.setFont(new Font("SansSerif", Font.BOLD, 100));
            g.setColor(Color.RED);
            g.drawString("YOU WIN", 200, 200);
            return;
        }
        Color c = g.getColor();
        g.setColor(Color.GREEN);
        g.fillRect(0, 0, 800, 600);
        missilehit(g);

        g.setColor(Color.RED);
        blood.draw(g);

        if (Tank.listExplode.size() != 0) {
            for (int i = 0; i < Tank.listExplode.size(); i++) {
                Tank.listExplode.get(i).draw(g);
            }
        }

        Tankwall(g);
        tank.paint(g);
        if (tank.getLife() <= 0) {
            tank.setLive(false);
        }
        if (tank.getRect().intersects(blood.getRect())) {
            tank.eat(blood);
        }
        g.setColor(Color.black);
        wall1.draw(g);
        wall2.draw(g);
        g.drawString("missiles count:" + Tank.listMissil.size(), 10, 50);
        g.drawString("explode count:" + Tank.listExplode.size(), 10, 70);
        g.drawString("tanks count: " + enemyTankList.size(), 10, 90);
        g.drawString("tank life: " + tank.getLife(), 10, 110);
        g.setColor(c);
    }

    public void update(Graphics g) {
        {
            if (offScreenImage == null) {
                offScreenImage = this.createImage(GAME_WIDTH, GAME_HIGHT);
            }
            Graphics gOffScreen = offScreenImage.getGraphics();
            Color c = gOffScreen.getColor();
            gOffScreen.setColor(Color.GREEN);
            gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HIGHT);
            gOffScreen.setColor(c);
            print(gOffScreen);
            g.drawImage(offScreenImage, 0, 0, null);
        }
    }

    public void LaunchJFrame() {
        this.setLocation(300, 100);
        this.setSize(GAME_WIDTH, GAME_HIGHT);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setBackground(Color.GREEN);
        this.addKeyListener(new KeyMonitor());
        setVisible(true);
        for (int i = 0; i < 10; i++) {
            enemyTankList.add(new Tank(50 + 40 * (i + 1), 50, false));
            enemyTankList.get(i).setLife(20);
        }
        new Thread(new PaintThread()).start();
    }

    private class KeyMonitor extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            tank.keyPressed(e);
        }

        public void keyReleased(KeyEvent e) {
            tank.keyReleased(e);
            if ((tank.getLife() == 0 ||enemyTankList.size()==0)&& e.getKeyCode() == KeyEvent.VK_F12) {
                restart();
            }
        }
    }

    void missilehit(Graphics g) {
        for (int i = 0; i < Tank.listMissil.size(); i++) {
            Tank.listMissil.get(i).paint(g);
            if (Tank.listMissil.get(i).hitTanks(enemyTankList) ||
                    Tank.listMissil.get(i).getY() > 600 ||
                    Tank.listMissil.get(i).getY() < 0 ||
                    Tank.listMissil.get(i).getX() < 0 ||
                    Tank.listMissil.get(i).getX() > 800) {
                Tank.listMissil.remove(i);
            } else if (Tank.listMissil.get(i).hitTank(tank)) {
                Tank.listMissil.remove(i);
            } else if (Tank.listMissil.get(i).hitWall(wall1)) {
                Tank.listMissil.remove(i);
            } else if (Tank.listMissil.get(i).hitWall(wall2)) {
                Tank.listMissil.remove(i);
            }

        }
    }

    void Tankwall(Graphics g) {
        for (int i = 0; i < enemyTankList.size(); i++) {
            enemyTankList.get(i).paint(g);
            enemyTankList.get(i).collidesWithTanks(enemyTankList);
            if (enemyTankList.get(i).collidesWithWall(wall1)) {
            } else if (enemyTankList.get(i).collidesWithWall(wall2)) {
            }
        }
        tank.collidesWithWall(wall1);
        tank.collidesWithWall(wall2);
        tank.collidesWithTanks(enemyTankList);
    }

    public class PaintThread implements Runnable {
        public void run() {
            while (true) {
                repaint();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void restart() {
        tank.setLive(false);
        tank = new Tank(50, 50, true);
        enemyTankList.clear();
        for (int i = 0; i < 10; i++) {
            enemyTankList.add(new Tank(50 + 40 * (i + 1), 350, false));
            enemyTankList.get(i).setLife(20);
        }
        Tank.listMissil.clear();
        Tank.listExplode.clear();
        blood.setLive(false);
        blood = new Blood();
    }

    public static void main(String[] args) {
        new TankClient().LaunchJFrame();
    }
}